import React, { Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { selectRecipe } from 'store/actions/actions';

import {
  SectionContainer,
  SectionTopWrapper,
  SectionTitle,
  SectionText,
  SectionContent,
} from 'GlobalStyles';

import { RecipeContainer, RecipeCard } from './RecipeListStyle';

import Recipe from 'components/Recipe/Recipe';

const RecipeList = (props) => {
  const recipes = useSelector((state) => state.recipes);
  const dispatch = useDispatch();

  let history = useHistory();
  let location = useLocation();

  const handleRecipeDetails = (recipe) => (event) => {
    dispatch(selectRecipe(recipe));
    history.push(`${location.pathname}/${recipe.id}`);
  };

  const listOfRecipes = () => {
    let recipeList = recipes.map((recipe, idx) => {
      return (
        <RecipeCard key={recipe.id} onClick={handleRecipeDetails(recipe)}>
          <Recipe recipe={recipe}></Recipe>
        </RecipeCard>
      );
    });

    return (
      <Fragment>
        <SectionTopWrapper>
          <SectionTitle>List of Recipes</SectionTitle>
          <SectionText>The list of available recipes</SectionText>
        </SectionTopWrapper>
        <SectionContent>
          <RecipeContainer>{recipeList}</RecipeContainer>
        </SectionContent>
      </Fragment>
    );
  };

  return <SectionContainer>{listOfRecipes()}</SectionContainer>;
};

export default RecipeList;
