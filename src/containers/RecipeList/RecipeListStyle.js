import styled from 'styled-components';

export const RecipeContainer = styled.div`
  display: grid;
  flex-grow: 1;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 20px;
`;

export const RecipeCard = styled.div`
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  overflow: hidden;

  background-color: white;
  transition: height 0.4s ease-out;
  box-shadow: 0px 2px 34px rgba(121, 121, 121, 0.5);

  transition: transform 0.2s;

  :hover {
    transform: scale(1.01);
  }
`;
