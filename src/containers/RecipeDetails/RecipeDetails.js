import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';

import { selectRecipe } from 'store/actions/actions';
import { TabPanel, isObjectNullOrEmpty } from 'GlobalFunctions';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import CircularProgress from '@material-ui/core/CircularProgress';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import {
  RecipeContainer,
  ImageContainer,
  RecipeWrapper,
  RecipeTitle,
  RecipeDescription,
  RecipeTopWrapper,
  RecipeContent,
  TabsContainer,
  RelatedRecipesContainer,
} from './RecipeDetailsStyle';

import Recipe from 'components/Recipe/Recipe';

const RecipeDetails = (props) => {
  let { recipeId } = useParams();
  const selectedRecipe = useSelector((state) => state.selectedRecipe);
  const recipesByIngredients = useSelector(
    (state) => state.recipesByIngredients
  );
  const dispatch = useDispatch();

  let history = useHistory();

  const [value, setValue] = useState(0);
  const [relatedRecipes, setRelatedRecipes] = useState([]);

  useEffect(() => {
    if (isObjectNullOrEmpty(selectedRecipe)) {
      dispatch(selectRecipe(recipeId));
    }
  }, [recipeId, dispatch, selectedRecipe]);

  useEffect(() => {
    if (!isObjectNullOrEmpty(selectedRecipe)) {
      setRelatedRecipes(getRelatedRecipes());
    } else {
      history.push(`/recipes-list`);
    }
  }, [selectedRecipe, history]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const getRelatedRecipes = () => {
    let relatedRecipes = [];

    selectedRecipe.ingredients.forEach((ingredient) => {
      const recipes = recipesByIngredients[ingredient.name];
      if (recipes) {
        relatedRecipes = relatedRecipes.concat(recipes);
      }
    });

    const uniqueArray = relatedRecipes.filter((thing, index) => {
      const _thing = JSON.stringify(thing);
      return (
        selectedRecipe.id !== thing.id &&
        index ===
          relatedRecipes.findIndex((obj) => {
            return JSON.stringify(obj) === _thing;
          })
      );
    });

    return uniqueArray;
  };

  const accessRecipe = (recipeId) => (event) => {
    dispatch(selectRecipe(recipeId));
    history.push(`/recipes-list/${recipeId}`);
  };

  return !isObjectNullOrEmpty(selectedRecipe) ? (
    <RecipeContainer>
      <ImageContainer>
        <img src={selectedRecipe.image} alt='recipe'></img>
      </ImageContainer>
      <RecipeWrapper>
        <RecipeTopWrapper>
          <RecipeTitle>{selectedRecipe.name}</RecipeTitle>
          <RecipeDescription>{selectedRecipe.description}</RecipeDescription>
        </RecipeTopWrapper>
        <RecipeContent>
          <TabsContainer>
            <AppBar position='static'>
              <Tabs value={value} onChange={handleChange}>
                <Tab label='Ingredients' id={0} />
                <Tab label='Steps' id={1} />
                <Tab label='Related recipes' id={2} />
              </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
              <List dense>
                {selectedRecipe.ingredients.map((elem, idx) => {
                  return (
                    <ListItem key={elem.name + idx}>
                      <ListItemText
                        primary={`${elem.amount} ${elem.unit} of ${elem.name}`}
                      />
                    </ListItem>
                  );
                })}
              </List>
            </TabPanel>
            <TabPanel value={value} index={1}>
              <List dense>
                {selectedRecipe.steps.map((elem, idx) => {
                  return (
                    <ListItem key={`step_${idx}`}>
                      <ListItemText primary={`${idx + 1}. ${elem}`} />
                    </ListItem>
                  );
                })}
              </List>
            </TabPanel>
            <TabPanel value={value} index={2}>
              <RelatedRecipesContainer>
                {relatedRecipes.length > 0 ? (
                  relatedRecipes.map((elem) => {
                    return (
                      <div
                        key={elem.id}
                        onClick={accessRecipe(elem.id)}
                        style={{ height: '360px' }}
                      >
                        <Recipe key={elem.id} recipe={elem}></Recipe>
                      </div>
                    );
                  })
                ) : (
                  <span>There are no related recipes!</span>
                )}
              </RelatedRecipesContainer>
            </TabPanel>
          </TabsContainer>
        </RecipeContent>
      </RecipeWrapper>
    </RecipeContainer>
  ) : (
    <CircularProgress />
  );
};

export default RecipeDetails;
