import styled from 'styled-components';

export const RecipeContainer = styled.div`
  display: flex;
  height: 100%;
`;

export const ImageContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  width: 50%;

  img {
    height: 100%;
    object-fit: cover;
  }
`;

export const RecipeWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 50%;
  background-color: black;
`;

export const RecipeTopWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 20%;
  padding: 20px;
`;

export const RecipeTitle = styled.h1`
  color: white;
  font-weight: normal;
  padding: 20px 20px 0 20px;
  box-sizing: border-box;
`;

export const RecipeDescription = styled.span`
  color: white;
  padding: 0 20px;
  box-sizing: border-box;
  font-size: 1.2em;
`;

export const RecipeContent = styled.div`
  display: flex;
  width: 100%;
  height: 80%;
`;

export const TabsContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;

  [class*='MuiAppBar-colorPrimary'] {
    background-color: black;
    padding: 0 10px;
  }

  > div {
    display: flex;
    height: calc(100% - 48px);
    background-color: white;
  }

  > div[hidden] {
    display: none;
    height: calc(100% - 48px);
    background-color: white;
  }
`;

export const RelatedRecipesContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 20px;
  grid-row-gap: 20px;
  padding-bottom: 20px;
`;
