import styled from 'styled-components';
import { globalVars } from 'GlobalStyles';

export const BodyContainer = styled.div`
  display: flex;
  flex-direction: column;

  height: calc(100vh - ${globalVars.headerHeight});
  margin-top: ${globalVars.headerHeight};
`;
