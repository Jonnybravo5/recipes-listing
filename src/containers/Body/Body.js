import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchRecipes } from 'store/actions/actions';

import { Route, Redirect } from 'react-router-dom';
import { Switch } from 'react-router-dom';

import { BodyContainer } from './BodyStyle';

import CircularProgress from '@material-ui/core/CircularProgress';
import RecipeList from 'containers/RecipeList/RecipeList';
import RecipeDetails from 'containers/RecipeDetails/RecipeDetails';

const Body = (props) => {
  const recipes = useSelector((state) => state.recipes);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchRecipes());
  }, [dispatch]);

  return (
    <BodyContainer>
      {recipes.length > 0 ? (
        <Switch>
          <Route exact path='/recipes-list' component={RecipeList} />

          <Route path='/recipes-list/:recipeId' component={RecipeDetails} />

          <Redirect to='/recipes-list' />
        </Switch>
      ) : (
        <CircularProgress />
      )}
    </BodyContainer>
  );
};

export default Body;
