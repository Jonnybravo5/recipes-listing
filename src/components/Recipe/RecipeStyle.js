import styled from 'styled-components';
import { withStyles } from '@material-ui/core/styles';

import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';

export const MuiCardMedia = styled(CardMedia)`
  height: 200px;
`;

export const MuiCardContent = withStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    height: '50%',
  },
})(CardContent);
