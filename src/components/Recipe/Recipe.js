import React from 'react';
import PropTypes from 'prop-types';

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import Typography from '@material-ui/core/Typography';

import { MuiCardMedia, MuiCardContent } from './RecipeStyle';

const Recipes = (props) => {
  const { recipe } = props;

  return (
    <Card style={{ height: '100%' }}>
      <CardActionArea style={{ height: '100%' }}>
        <MuiCardMedia
          image={recipe.image}
          src={recipe.image}
          height='140px'
          title={recipe.name}
        />
        <MuiCardContent>
          <Typography gutterBottom variant='h5' component='h2'>
            {recipe.name}
          </Typography>
          <Typography variant='body2' color='textSecondary' component='p'>
            {recipe.description}
          </Typography>
        </MuiCardContent>
      </CardActionArea>
    </Card>
  );
};

Recipes.propTypes = {
  recipe: PropTypes.object,
};

export default Recipes;
