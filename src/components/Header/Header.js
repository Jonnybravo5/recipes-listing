import React, { Fragment, useState, useEffect } from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import IconButton from '@material-ui/core/IconButton';

import {
  HeaderStyle,
  HeaderWrapper,
  BackContainer,
  Logo,
  LogoImage,
  ButtonsContainer,
  StyledButton,
} from './HeaderStyle';

import AddNewRecipe from 'components/AddNewRecipe/AddNewRecipe';

import logo from 'assets/img/logo.svg';

const Header = (props) => {
  const [openCart, setOpenCart] = useState(false);
  let history = useHistory();

  let match = useRouteMatch({
    path: '/recipes-list',
    strict: true,
    sensitive: true,
  });

  useEffect(() => {});

  const handleClickOpen = () => {
    setOpenCart(true);
  };

  const handleClose = (value) => {
    setOpenCart(false);
  };

  const handleBackButton = () => {
    history.push(`/recipes-list`);
  };

  return (
    <Fragment>
      <HeaderStyle>
        <HeaderWrapper>
          <BackContainer onClick={handleBackButton}>
            {match && !match.isExact && (
              <Fragment>
                <IconButton aria-label='expand row' size='small'>
                  {<ArrowBackIcon />}
                </IconButton>
                <span>List of Recipes</span>
              </Fragment>
            )}
          </BackContainer>
          <Logo>
            <LogoImage src={logo} alt='logo' />
          </Logo>

          <ButtonsContainer>
            <StyledButton
              variant='contained'
              color='secondary'
              startIcon={<FavoriteIcon />}
              onClick={handleClickOpen}
            >
              Add new Recipe
            </StyledButton>
          </ButtonsContainer>
        </HeaderWrapper>
      </HeaderStyle>
      <AddNewRecipe open={openCart} onClose={handleClose}></AddNewRecipe>
    </Fragment>
  );
};

export default Header;
