import styled from 'styled-components';
import { theme } from 'GlobalStyles';
import { withStyles } from '@material-ui/core/styles';

import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import TextField from '@material-ui/core/TextField';

export const DialogHeaderWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const MuiDialogTitle = styled(DialogTitle)`
  h2 {
    font-size: 1.7em;
    color: ${theme.primary};
    font-weight: 600;
  }
`;

export const MuiDialogContent = styled(DialogContent)`
  display: flex;
  flex-direction: column;

  > div {
    padding-bottom: 15px;
  }
`;

export const MuiDialogActions = styled(DialogActions)`
  button {
    color: ${theme.primary};
    font-weight: bold;
  }
`;

export const MuiListItem = styled(ListItem)`
  &.MuiListItem-root {
    display: flex;
    justify-content: space-evenly;
    align-items: center;
  }
`;

export const MuiListItemText = styled(ListItemText)`
  max-width: 70%;
  span {
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
  }
`;

export const MuiTextField = withStyles({
  root: {
    '& .MuiInput-underline:after': {
      borderBottomColor: theme.primary,
    },
    '& .MuiInputLabel-root.Mui-focused': {
      color: theme.primary,
    },
  },
})(TextField);

export const AddIngredientContainer = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  padding: 10px;

  button {
    background-color: orange;

    :hover {
      background-color: ${theme.primary};
    }
  }
`;

export const TotalContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 20px;

  font-size: 1.2em;
  font-weight: bold;

  border-top: 1px solid grey;
`;
