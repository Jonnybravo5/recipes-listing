import React, { useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { addNewRecipe } from 'store/actions/actions';
import { TabPanel } from 'GlobalFunctions';
import { TabsContainer } from 'GlobalStyles';

import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import {
  MuiDialogTitle,
  MuiDialogContent,
  MuiDialogActions,
  MuiListItem,
  MuiTextField,
  AddIngredientContainer,
} from './AddNewRecipeStyle';

import Ingredient from 'components/Ingredient/Ingredient';

const AddNewRecipe = (props) => {
  const { onClose, open } = props;
  const dispatch = useDispatch();

  const [value, setValue] = useState(0);

  const [imageUrl, setImageUrl] = useState('');
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [ingredients, setIngredients] = useState([]);

  const handleClose = () => {
    setImageUrl('');
    setTitle('');
    setDescription('');
    setIngredients([]);
    onClose();
  };

  const createRecipe = () => {
    let recipe = {
      name: title,
      id: createId(title),
      description: description,
      image: imageUrl,
      ingredients: ingredients,
      steps: [], // same logic implemented for the ingredients
    };

    dispatch(addNewRecipe(recipe));
    handleClose();
  };

  const createId = (txt) => {
    return txt
      .toLowerCase()
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
      .replace(/[^\w\s]|_/g, '')
      .replace(/\s/g, '-');
  };

  const handleAddIngredient = (event) => {
    setIngredients((ing) => [
      ...ing,
      {
        amount: '',
        unit: '',
        name: '',
      },
    ]);
  };

  const handleRemoveIngredient = (ingIdx) => (event) => {
    setIngredients(ingredients.filter((ing, idx) => idx !== ingIdx));
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const tabsElement = () => {
    return (
      <Fragment>
        <AppBar position='static'>
          <Tabs value={value} onChange={handleChange}>
            <Tab label='Ingredients' id={0} />
            <Tab label='Steps' id={1} />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <AddIngredientContainer>
            <Fab
              variant='extended'
              size='small'
              color='primary'
              aria-label='add'
              onClick={handleAddIngredient}
            >
              <AddIcon />
              Add ingredients
            </Fab>
          </AddIngredientContainer>
          <List style={{ maxHeight: '150px' }}>
            {ingredients.map((ingredient, idx) => {
              return (
                <MuiListItem key={`ingredient_${idx}`}>
                  <Ingredient
                    idx={idx}
                    ingredient={ingredient}
                    handleRemoveIngredient={handleRemoveIngredient}
                  ></Ingredient>
                </MuiListItem>
              );
            })}
          </List>
        </TabPanel>
        <TabPanel value={value} index={1}>
          (Same logic implemented for the ingredients)
        </TabPanel>
      </Fragment>
    );
  };

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby='simple-dialog-title'
      open={open}
      maxWidth='sm'
      fullWidth={true}
    >
      <MuiDialogTitle id='simple-dialog-title'>Add new Recipe</MuiDialogTitle>
      <MuiDialogContent>
        <MuiTextField
          id='image'
          label='Image URL'
          onChange={(e) => setImageUrl(e.target.value)}
          value={imageUrl}
        />
        <MuiTextField
          id='title'
          label='Title'
          onChange={(e) => setTitle(e.target.value)}
          value={title}
        />
        <MuiTextField
          id='description'
          label='Description'
          onChange={(e) => setDescription(e.target.value)}
          value={description}
        />
        <TabsContainer>{tabsElement()}</TabsContainer>
      </MuiDialogContent>
      <MuiDialogActions>
        <Button onClick={handleClose} color='primary'>
          Cancel
        </Button>
        <Button onClick={createRecipe} color='primary' autoFocus>
          Add
        </Button>
      </MuiDialogActions>
    </Dialog>
  );
};

AddNewRecipe.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
};

export default AddNewRecipe;
