import React, { useEffect, useState, Fragment } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import DeleteIcon from '@material-ui/icons/Delete';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

import { DeleteContainer } from './IngredientStyle';

const Ingredient = (props) => {
  const { idx, ingredient, handleRemoveIngredient } = props;
  const ingredients = useSelector((state) => state.ingredients);
  const units = useSelector((state) => state.units);

  const [ingredientItem, setIngredientItem] = useState(ingredient);

  useEffect(() => {
    setIngredientItem(ingredient);
  }, [ingredient]);

  const handleChange = (property) => (event) => {
    const { value } = event.target;
    ingredient[property] = value;

    setIngredientItem((ingredientItem) => {
      return {
        ...ingredientItem,
        [property]: value,
      };
    });
  };

  return (
    <Fragment>
      <TextField
        id='amount'
        label='Amount'
        type='number'
        value={ingredientItem.amount}
        onChange={handleChange('amount')}
        style={{ width: '100px' }}
      />
      <FormControl style={{ width: '100px' }}>
        <InputLabel id='unit'>Unit</InputLabel>
        <Select
          labelId='unit'
          id='unit'
          value={ingredientItem.unit}
          onChange={handleChange('unit')}
        >
          {units.map((unit, idx) => {
            return (
              <MenuItem key={`unit_${idx}`} value={unit}>
                {unit}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
      <FormControl style={{ width: '200px' }}>
        <InputLabel id='name'>Name</InputLabel>
        <Select
          labelId='name'
          id='name'
          value={ingredientItem.name}
          onChange={handleChange('name')}
        >
          {ingredients.map((ingredient, idx) => {
            return (
              <MenuItem key={`name_${idx}`} value={ingredient}>
                {ingredient}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
      <DeleteContainer>
        <DeleteIcon
          onClick={handleRemoveIngredient(idx)}
          style={{ cursor: 'pointer' }}
        />
      </DeleteContainer>
    </Fragment>
  );
};

Ingredient.propTypes = {
  ingredient: PropTypes.object,
  handleRemoveIngredient: PropTypes.func,
};

export default Ingredient;
