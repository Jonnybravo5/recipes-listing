import styled from 'styled-components';

export const DeleteContainer = styled.span`
  display: flex;
  padding: 5px;
  align-self: flex-end;
`;
