import React from 'react';
import { ThemeProvider } from 'styled-components';
import { AppContainer, GlobalStyle, theme } from 'GlobalStyles';

import Header from 'components/Header/Header';
import Body from 'containers/Body/Body';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <AppContainer style={{ display: 'flex', flexDirection: 'column' }}>
        <GlobalStyle />
        <Header></Header>
        <Body></Body>
      </AppContainer>
    </ThemeProvider>
  );
}

export default App;
