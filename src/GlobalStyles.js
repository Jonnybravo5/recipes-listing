import styled, { createGlobalStyle } from 'styled-components';

export const theme = {
  primary: '#da7016',
  secondary: 'gray',
  background: '#DEEEF2',
  textColor: '#919497',
};

export const globalVars = {
  headerHeight: '70px',
  headerWidthPercentage: '100%',
};

export const AppContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const SectionContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
  padding-bottom: 40px;
  box-sizing: border-box;
  overflow: auto;
`;

export const SectionTopWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 20px;
`;

export const SectionTitle = styled.h1`
  color: ${theme.primary};
  padding: 20px 20px 0 20px;
  box-sizing: border-box;
`;

export const SectionText = styled.span`
  color: ${theme.textColorDark};
  padding: 0 20px;
  box-sizing: border-box;
  font-size: 1.2em;

  strong {
    font-weight: 600;
  }
`;

export const SectionContent = styled.div`
  display: ${(props) => (props.hidden ? 'hidden' : 'flex')};
  flex-direction: column;
  padding: 0px 20px 40px 40px;
`;

export const SectionFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 15px 25px;
  box-sizing: border-box;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
`;

export const TabsContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;

  [class*='MuiAppBar-colorPrimary'] {
    background-color: black;
    padding: 0 10px;
  }

  > div {
    display: flex;
    height: calc(100% - 48px);
    background-color: white;
  }

  > div[hidden] {
    display: none;
    height: calc(100% - 48px);
    background-color: white;
  }
`;

export const GlobalStyle = createGlobalStyle`
  html {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
  }

  body {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin: 0;
    padding: 0;
    font-size: 16px;
    font-family: "Roboto", sans-serif;
    letter-spacing: 0.05rem;
    background-color: rgba(222, 224, 233, 0.01);
    line-height: 28px;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    overflow: hidden;
  }

  h1 {
    font-size: 2.375em;
    font-weight: bold;
    color: ${(props) => props.theme.textColor};
    line-height: 1.5em;
    margin: 0;
    padding-bottom: 25px;
    box-sizing: border-box;
  }

  h2 {
    font-size: 2em;
    font-weight: 500;
    color: ${(props) => props.theme.textColor};
    line-height: 1.2em;
    margin: 0;
    padding: 15px 0;
    box-sizing: border-box;
  }

  h3 {
    font-size: 1.2em;
    font-weight: 500;
    color: ${(props) => props.theme.textColor};
    line-height: 1.2em;
    margin: 0;
    box-sizing: border-box;
  }

  a{
    text-decoration: none;
    color: ${(props) => props.theme.primary};
    font-weight: bold;
  }

  p, ol {
    margin: 0;
  }

  .blue-text {
    color: ${(props) => props.theme.primary};
  }
`;
