import axios from 'axios';

import { FETCH_RECIPES, ADD_NEW_RECIPE, SELECT_RECIPE } from './action-types';

export const fetchRecipes = () => async (dispatch) => {
  const response = await axios.get('/recipes.json');

  dispatch({
    type: FETCH_RECIPES,
    payload: response.data,
  });
};

export const addNewRecipe = (recipe) => {
  return {
    type: ADD_NEW_RECIPE,
    payload: recipe,
  };
};

export const selectRecipe = (recipe) => {
  return {
    type: SELECT_RECIPE,
    payload: recipe,
  };
};
