import {
  FETCH_RECIPES,
  ADD_NEW_RECIPE,
  SELECT_RECIPE,
} from 'store/actions/action-types';

const initialState = {
  recipes: [],
  recipesByIngredients: {},
  ingredients: [
    'pasta flour',
    'eggs',
    'egg yolk',
    'semolina flour',
    'peanut butter',
    'golden sugar',
    'table salt',
    'plain flour',
    'butter',
    'sugar',
    'orange',
    'star anise',
    'toasted coconut',
    'chopped parsley',
    'red onion',
    'lime juice',
  ],
  units: ['cup', 'tsp', 'tbsp', 'unit', 'gr', 'ml', 'pinch'],
  selectedRecipe: {},
};

const recipesReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case FETCH_RECIPES:
      return {
        ...state,
        recipes: payload,
        recipesByIngredients: mapRecipesByIngredient(payload),
      };
    case ADD_NEW_RECIPE:
      return {
        ...state,
        recipes: [...state.recipes, payload],
        recipesByIngredients: updateRecipesByIngredient(
          state.recipesByIngredients,
          payload
        ),
      };
    case SELECT_RECIPE:
      if (typeof payload === 'string') {
        return {
          ...state,
          selectedRecipe: state.recipes.find((recipe) => {
            return recipe.id === payload;
          }),
        };
      }

      return {
        ...state,
        selectedRecipe: payload,
      };
    default:
  }

  return state;
};

const mapRecipesByIngredient = (recipes) => {
  const map = {};

  recipes.forEach((recipe) => {
    recipe.ingredients.forEach((ingredient) => {
      if (!map[ingredient.name]) map[ingredient.name] = [];
      map[ingredient.name].push(recipe);
    });
  });

  return map;
};

const updateRecipesByIngredient = (recipesByIngredients, recipe) => {
  const map = { ...recipesByIngredients };

  recipe.ingredients.forEach((ingredient) => {
    if (!map[ingredient.name]) map[ingredient.name] = [];
    map[ingredient.name].push(recipe);
  });

  return map;
};

export default recipesReducer;
